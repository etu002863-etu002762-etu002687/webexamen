<?php
    function dbconnect()
	{
    	static $connect = null;
    	if ($connect === null)
    	{
    	    $connect = mysqli_connect('localhost','root','','the');
    	}
    	return $connect;
	}
    function checkLogin($nom,$pass)
    {
        $sql="SELECT pseudo_Utilisateur FROM Utilisateur WHERE pseudo_Utilisateur='%s' AND mdp='%s'";
        $sql=sprintf($sql,$nom,$pass);
        $requete=mysqli_query(dbconnect(),$sql);
        $resultat=mysqli_fetch_assoc($requete);
        $n=mysqli_num_rows($requete);
        if($n == 0)
        {
            header('location:login.html');
        }   
        else
        {
            header('location:ceuillette.html');                            
        }
    }
    function insertSaisie($date,$cueilleur,$parcelle,$poids)
	{
		$sql="insert into saisie_cueillette(date_quantite_recueilli,id_Cueilleur,id_numParcelle,quantite_Recueilli) values('".$date."',".$cueilleur.",".$parcelle.",".$poids.")";
		mysqli_query(dbconnect(),$sql);
	}
    function insertDepenses($date,$categorie,$montant)
    {
        $sql="insert into saisie_depense(date_de_depense,id_depense,montant_depense) values('".$date."',".$categorie.",".$montant.")";
    }
    function PoidsTotal()
    {
        $sql="select sum(quantite_Recueilli) as Poids_Total from saisie_cueillette";
        $resultat=mysqli_query(dbconnect(),$sql);
        if (!$resultat) {
            echo "Erreur lors de l'exécution de la requête : " . mysqli_error($connexion);
            return;
        }
        $row = mysqli_fetch_assoc($resultat);
        $poidsTotal = $row['Poids_Total'];
    
        echo "Le poids total est : " . $poidsTotal;
    
        mysqli_free_result($resultat);
    
        mysqli_close(dbconnect());
    }
    function calculerPoidsRestant($idParcelle) {
        // Connexion à la base de données
        $connexion = dbconnect();
    
        // Requête SQL pour obtenir la somme de la quantité recueillie pour la parcelle donnée
        $sql = "SELECT SUM(quantite_Recueilli) as quantite_totale FROM saisie_cueillette WHERE id_numParcelle = $idParcelle";
    
        // Exécution de la requête
        $resultat = mysqli_query($connexion, $sql);
    
        // Vérification des erreurs
        if (!$resultat) {
            echo "Erreur lors de l'exécution de la requête : " . mysqli_error($connexion);
            return;
        }
    
        // Récupération du résultat
        $row = mysqli_fetch_assoc($resultat);
        $quantiteTotale = $row['quantite_totale'];
    
        // Requête SQL pour obtenir la surface totale de la parcelle donnée
        $sql_surface = "SELECT surface_en_hectare FROM saisie_cueillette WHERE id_numParcelle = $idParcelle";
        $resultat_surface = mysqli_query($connexion, $sql_surface);
        $row_surface = mysqli_fetch_assoc($resultat_surface);
        $surfaceParcelle = $row_surface['surface_en_hectare'];
    
        // Calcul du poids restant
        $poidsRestant = $surfaceParcelle - $quantiteTotale;
    
        // Affichage du résultat
        echo "Le poids restant pour la parcelle $idParcelle est : $poidsRestant";
    
        // Libération des ressources
        mysqli_free_result($resultat);
        mysqli_free_result($resultat_surface);
    
        // Fermeture de la connexion
        mysqli_close($connexion);
    }
    function calculDeRevient($idParcelle, $poidsRestant) {
        $coutMatierePremiereParKilo = 2;
        $coutMainOeuvreParHeure = 15;
        $dureeTravail = 4;
        $autresFrais = 50;
    
        $coutMainOeuvre = $coutMainOeuvreParHeure * $dureeTravail;
        $coutMatierePremiere = $poidsRestant * $coutMatierePremiereParKilo;
    
        $coutTotal = $coutMatierePremiere + $coutMainOeuvre + $autresFrais;
    
        echo "Le coût de revient pour la parcelle $idParcelle avec un poids restant de $poidsRestant kilogrammes est : $coutTotal";
    }
    function poidsMinimalJournalier($poidsRestant, $joursDisponibles) {
        if ($joursDisponibles <= 0) {
            return 0;
        }
        $poidsMinimal = $poidsRestant / $joursDisponibles;
        return $poidsMinimal;
    }
    function MonthsRegenerate($idParcelle,$poidsRestant)
    {
        $poidsRestant=calculerPoidsRestant($idParcelle);
        if($mois==false)
        {
            return $poidsRestant;
        }
        else
        {
            return $poidsRestant++;
        }
    }
    function calculerMontantVente($quantiteVendue, $prixUnitaire) {
        // Calcul du montant de la vente en multipliant la quantité vendue par le prix unitaire
        $montantVente = $quantiteVendue * $prixUnitaire;
        return $montantVente;
    }
    function calculerPrevision($date) {
        
        $dateObj = new DateTime($date);
    
        $dateObj->modify('+7 days');
    
        return $dateObj->format('Y-m-d');
    }
?>