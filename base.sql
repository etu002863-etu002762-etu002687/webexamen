create database the;
    use the;


create table Utilisateur 
(
    id_Utilisateur int auto_increment,
    pseudo_Utilisateur varchar(50),
    mdp varchar(50),
    primary key (id_Utilisateur)
);

insert into Utilisateur value (null, 'rakotoTiaT', 'rtt5678');
insert into Utilisateur value (null, 'TiaTinfuserZa', 'ttiz18');
insert into Utilisateur value (null, 'Tlover', 'tl12345');

create table Variete_the_plante 
(
    id_variete int,
    nom_variete_the_plante varchar(50),
    surface_en_hectare int,
    prix_unitaire int, 
    primary key (id_variete)
);

insert into Variete_the_plante values (null,'The noir',1000,15000);
insert into Variete_the_plante values (null,'The vert',1000,10000);
insert into Variete_the_plante values (null,'The rose,au fleur dhibiscus',1000,20000);
insert into Variete_the_plante values (null,'The blanc',1000,10000);
insert into Variete_the_plante values (null,'The jaune,alias le the imperial',1000,20000);
insert into Variete_the_plante values (null,'The aromatise',1000,15000);
insert into Variete_the_plante values (null,'The nature',1000,10000);
insert into Variete_the_plante values (null,'The detox',1000,20000);


create table Cueilleur 
(
    id_Cueilleur int auto_increment,
    nom_Cueilleur varchar(50),
    prenom_Cueilleur varchar(50),
    variete_the_plante varchar(50),
    quantite_Recueilli int,
    numero_parcelle int,
    surface_en_hectare int,
    primary key (id_Cueilleur)
);

insert into Cueilleur value (null, 'Rakoto', 'Malala','The noir',1,1,1000);
insert into Cueilleur value (null, 'Ravao', 'Marie','The vert',2,2,1000);
insert into Cueilleur value (null, 'Rivo', 'Lala','The rose,au fleur dibiscus',3,3,1000);
insert into Cueilleur value (null, 'Marie', 'Jeanne','The jaune,alias le the imperial',4,4,1000);
insert into Cueilleur value (null, 'Andria', 'Mi','The blanc',5,5,1000);
insert into Cueilleur value (null, 'Rak', 'Paul','The aromatise',6,6,1000);
insert into Cueilleur value (null, 'Adrian', 'Adrienne','The detox',6,6,1000);
insert into Cueilleur value (null, 'Ramaromahy', 'Tovo','The nature',7,7,1000);

create table Gestion_Partielle 
(
    id_numParcelle int auto_increment, 
    surface_en_hectare int, 
    variete_the_plante varchar(50),
    date_debut date,
    date_fin date,
    primary key (id_numParcelle)
);

insert into Gestion_Partielle values (null,1000,'The noir');
insert into Gestion_Partielle values (null,1000,'The vert');
insert into Gestion_Partielle values (null,1000,'The rose,au fleur dhibiscus');
insert into Gestion_Partielle values (null,1000,'The jaune,alias le the imperial');
insert into Gestion_Partielle values (null,1000,'The blanc');
insert into Gestion_Partielle values (null,1000,'The aromatise');
insert into Gestion_Partielle values (null,1000,'The detox');
insert into Gestion_Partielle values (null,1000,'The nature');

create table saisie_cueillette 
(
    id_saisie_cueillette int auto_increment, 
    date_quantite_recueilli date,
    id_Cueilleur int,
    quantite_Recueilli decimal,
    id_numParcelle int,
    surface_en_hectare int,
    primary key (id_saisie_cueillette)
);

create table saisie_depense 
(
    date_de_depense date,
    id_depense int auto_increment, 
    montant_depense decimal,
    primary key (id_depense)
);

create table categorie_depense 
(
    id_categorie_depense int auto_increment, 
    nom_categorie_depense varchar(100),
    primary key (id_categorie_depense)
);

insert into categorie_depense values (null,'engrais');
insert into categorie_depense values (null,'carburant');
insert into categorie_depense values (null,'logistique');